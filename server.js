var express = require('express');
var app = express();
var PORT = process.env.PORT || 3000;
var staticSite = __dirname+ "/dist";

app.use(express.static(staticSite));

app.listen(PORT, function(){
	console.log('Express started at port: ' +PORT);
});