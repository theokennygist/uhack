'use strict';

/**
 * @ngdoc function
 * @name uHackApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the uHackApp
 */
angular.module('uHackApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
