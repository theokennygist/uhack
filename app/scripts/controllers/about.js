'use strict';

/**
 * @ngdoc function
 * @name uHackApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the uHackApp
 */
angular.module('uHackApp')
  .controller('AboutCtrl', function ($scope) {
    var vm = $scope;

    vm.profiles = [{
    		name: 'Dhaniel Lukman',
    		roles: 'Business Intelligence',
    		image: '../../images/dhaniel.png'
    	},
    	{
    		name: 'Indira Venkatraman',
    		roles: 'Business Analyst',
    		image: '../../images/indi.png'
    	},
    	{
    		name: 'Kenny Erwin Soetjipto',
    		roles: 'Web Developer',
    		image:'../../images/kenny1.png'
    	},
    	{
    		name: 'Setia Budi',
    		roles: 'Data Intelligence',
    		image: '../../images/budi.png'
    	},
    	{
    		name: 'Ferry Susanto',
    		roles: 'Data Intelligence',
    		image: '../../images/ferry.png'
    	},
    	{
    		name: 'Rishona Shantapriyan',
    		roles: 'Data Analyst',
    		image: '../../images/little1.png'
    	}];
    console.log(this.profiles);
  });
